## JavaScript的this  

点击阅读：https://gitee.com/xv700/Web/blob/master/04-JavaScript%E8%BF%9B%E9%98%B6/this.md  


## call、apply、bind  

点击阅读：https://gitee.com/mody2020/Web/blob/master/04-JavaScript%E8%BF%9B%E9%98%B6/call%E3%80%81apply%E3%80%81bind%E7%9A%84%E5%8C%BA%E5%88%AB.md
 

## 作用域和闭包  

点击阅读：https://gitee.com/mody2020/Web/blob/master/04-JavaScript%E8%BF%9B%E9%98%B6/%E5%88%9B%E5%BB%BA%E5%AF%B9%E8%B1%A1%E5%92%8C%E7%BB%A7%E6%89%BF.md  


## 创建对象和继承

点击阅读：https://gitee.com/mody2020/Web/blob/master/04-JavaScript%E8%BF%9B%E9%98%B6/%E5%88%9B%E5%BB%BA%E5%AF%B9%E8%B1%A1%E5%92%8C%E7%BB%A7%E6%89%BF.md

## 浅拷贝和深拷贝

点击阅读：https://gitee.com/mody2020/Web/blob/master/04-JavaScript%E8%BF%9B%E9%98%B6/%E6%B5%85%E6%8B%B7%E8%B4%9D%E5%92%8C%E6%B7%B1%E6%8B%B7%E8%B4%9D.md